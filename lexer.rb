require "strscan"
require_relative "errors"

module W
	Token = Struct.new :type, :value, :lineno
	
	class Lexer
		TOKEN_REGEXPS = {
			id: /[_a-z]\w*/i,
			decimal_int: /-?\d+/,
			string: /"(?:\\"|[^"])*?"/,
			colon: /:/,
			lparen: /\(/,
			rparen: /\)/,
			comma: /,/
		}
	
		WHITESPACE = /[ \t\f\v]+/
	
		def initialize(input)
			@input = input
		end
		
		# Returns the token stream corresponding to the input text, as an array of Token objects.
		def lex
			tokens = []
			indent_levels = [0]
			lineno = 1
			@input.each_line do |line|
				line.chomp!
				# Skip completely empty lines
				next if line =~ /^\s*$/
				indent_level = line[/^\t*/].length
				# Parse the indentation level; increases generate INDENT tokens
				if indent_level > indent_levels.last
					indent_levels << indent_level
					tokens << Token.new(:indent, nil, lineno)
				else
					until indent_levels.last == indent_level
						if indent_levels.empty?
							raise SyntaxError, "line #{lineno}: indent level does not match any previous level"
						end
						indent_levels.pop
						tokens << Token.new(:dedent, nil, lineno)
					end
				end
				scanner = StringScanner.new line
				until (scanner.skip WHITESPACE; scanner.eos?)
					found = false
					TOKEN_REGEXPS.each do |type, regexp|
						if token = scanner.scan(regexp)
							found = true
							tokens << Token.new(type, token, lineno)
						end
					end
					if not found
						raise SyntaxError, "no valid token found in line #{lineno}, #{scanner.pos}: #{scanner.peek(10)}..."
					end
				end
				tokens << Token.new(:endline, nil, lineno)
				lineno += 1
			end
			# Generate dedents for the indented blocks that are left at the end
			(indent_levels.length - 1).times {tokens << Token.new(:dedent, nil, lineno)}
			tokens
		end
	end
end