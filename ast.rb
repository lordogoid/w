module W
	module AST
		class Integer
			def initialize(value)
				@value = value
			end
		end
		
		class String
			def initialize(value)
				@value = value
			end
		end
		
		class ID
			def initialize(id)
				@id = id
			end
		end
		
		class If
			def initialize(condition, true_branch, false_branch = nil)
				@condition = condition
				@true_branch = true_branch
				@false_branch = false_branch
			end
		end
		
		class While
			def initialize(condition, body)
				@condition = condition
				@body = body
			end
		end
		
		class Block
			def initialize(statements)
				@statements = statements
			end
		end
	end
end