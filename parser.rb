require "continuation"
require_relative "errors"
require_relative "ast"

module W
	class Parser
		# Creates a new parser from a given token array.
		def initialize(tokens)
			@tokens = tokens
			@pos = 0
			# When each AST node it is created, it is pushed here; each rule should only add at most
			# one node to the stack. Nodes are combined by popping one or more nodes off the stack and pushing
			# a composite one. When a rule fails, any nodes it pushed are removed.
			@ast_stack = []
			@choice_points = []
		end
		
		# Resets the parser to its initial state.
		def reset
			@pos = 0
			@ast_stack.clear
			@choice_points.clear
		end
		
		# If the next token is of type +token_type+, returns it and advances the pointer. Otherwise, backtracks.
		def get(token_type)
			if @pos < @tokens.length and @tokens[@pos].type == token_type
				token = @tokens[@pos]
				@pos += 1
				token
			else
				#STDERR.puts "failed to get #{token_type} at #{@pos} (#{@choice_points.length})"
				backtrack
			end
		end
		
		# Returns true (and advances the pointer) the next token is the given identifier. Otherwise, backtracks.
		def keyword(kw)
			(get(:id).value == kw) or backtrack
		end
		
		# Calls one of the given lambdas, calling the next one not yet called when backtracking. Sets a choice point. If the choices given are exhausted, backtracks.
		def choice(*funcs)
			funcs.each do |func|
				callcc do |cc|
					@choice_points << [@pos, @ast_stack.dup, cc]
					return func.call
				end
			end
			backtrack
		end
		
		# Restarts execution from the last choice point, selecting the next option available at that point.
		# If there are no choice points, throws a SyntaxError.
		def backtrack
			raise SyntaxError, "No choices left" if @choice_points.empty?
			@pos, @ast_stack, cc = @choice_points.pop
			cc.call
		end
		
		# Returns the parse tree for this parser's token array.
		def parse
			block
			if @pos != @tokens.length
				raise SyntaxError, "failed to consume entire input, stopped at line #{@tokens[@pos].lineno}"
			end
			@ast_stack.pop
		end
		
		def block
			start = @ast_stack.length
			loop do
				statement
				choice -> {}, proc {
					# Grab all AST nodes pushed since the beginning of the loop and put them in a block
					statements = @ast_stack[start..-1]
					@ast_stack.slice! start..-1
					@ast_stack << AST::Block.new(statements)
					return true
				}
			end
			true
		end
		
		def indented_block
			get :indent; block; get :dedent
		end
		
		def statement
			choice -> {if_statement}, -> {while_statement}, -> {expression; get :endline}
		end
		
		def expression
			choice -> {int}, -> {string}, -> {id}
		end
		
		def int
			token = get :decimal_int
			@ast_stack << AST::Integer.new(token.value.to_i)
		end
		
		def string
			token = get :string
			@ast_stack << AST::String.new(token.value)
		end
		
		def id
			token = get :id
			@ast_stack << AST::ID.new(token.value)
		end
		
		def if_statement
			start = @ast_stack.length
			keyword "if"; expression; get :colon; get :endline; indented_block
			catch :stop do
				loop do
					keyword "else"; keyword "if"; expression; get :colon; get :endline; indented_block
					choice -> {}, -> {throw :stop}
				end
			end
			choice -> {keyword "else"; get :colon; get :endline; indented_block}, -> {}
			pieces = @ast_stack.pop(@ast_stack.length - start)
			# If an else block exists, create a node for it, then build nodes for each if and else if statement
			node = AST::If.new(*pieces.pop(pieces.length.odd? ? 3 : 2))
			pieces.each_slice(2).reverse_each do |cond, branch|
				node = AST::If.new(cond, branch, node)
			end
			@ast_stack << node
		end
		
		def while_statement
			keyword "while"; expression; get :colon; get :endline; indented_block
			condition, body = @ast_stack.pop 2
			@ast_stack << AST::While.new(condition, body)
		end
	end
end

if __FILE__ == $0
	require "pp"
	require_relative "lexer"
	pp W::Parser.new(W::Lexer.new($<.read).lex).parse
end